import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { HomeGuardGuard } from './home-guard.guard';
import { LameHomeComponent } from './lame-home/lame-home.component';
import { ResolverService } from './resolver.service';


const routes: Routes = [
  {
    path: '',
    component: LameHomeComponent,
  },
  {
    path: 'home',
    component: HomeComponent,
    // resolve: {
    //   nothing: ResolverService
    // }
    canActivate: [HomeGuardGuard],
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {urlUpdateStrategy: 'eager',  relativeLinkResolution: 'corrected', initialNavigation: 'enabled' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
