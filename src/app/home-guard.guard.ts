import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { Location } from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class HomeGuardGuard implements CanActivate {
  constructor(private router: Router, private location: Location) {}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): any {
    // this.location.replaceState('home');
    this.router.config.unshift({path: 'home', component: HomeComponent});
    console.log(this.router.config);
    this.router.navigateByUrl('home', );
    return false;
  }

}
